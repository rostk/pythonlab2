class KitchenManager:
    appliance = []

    def __init__(self):
        pass

    def sort_by_power(self):
        self.appliance.sort(key=lambda KitchenAppliance: KitchenAppliance.power)

    def find_by_compatibility(self, dishes):
        found_appliance = []

        for KitchenAppliance in self.appliance:
            if KitchenAppliance.power == power:
                found_appliance.append(KitchenAppliance)

        return found_appliance

    def add_appliance(self, KitchenAppliance):
        self.appliance += KitchenAppliance

    def print_list(self):
        for KitchenAppliance in self.appliance:
            print(KitchenAppliance)
        print("\n")
