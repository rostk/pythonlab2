from Toaster import *
from Mixer import *
from KitchenManager import *
from Juicer import *
from Oven import *


if __name__ == '__main__':
    kitchenManager = KitchenManager()

    toaster = Toaster( 20, 30,"Toaster","Bread", DishesType.BREAD);
    mixer = Mixer( 13,50.45,"Mixer","Deserts", DishesType.DESERTS);
    juicer = Juicer(30,100.32, "Juicer","Juice",DishesType.JUICE);
    oven = Oven( 40,60.42,"Oven","Cakes", DishesType.CAKES);
    kitchenManager.appliance = [toaster, mixer, juicer, oven]
    print("Previous list:")
    kitchenManager.print_list()

    kitchenManager.sort_by_power()
    print("Sorted list")
    kitchenManager.print_list()

    pass
