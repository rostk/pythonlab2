from enum import Enum


class DishesType(Enum):
    JUICE = 1
    BREAD = 2
    CAKES = 3
    DESERTS = 4
