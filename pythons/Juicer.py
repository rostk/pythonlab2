from KitchenAppliance import *
from DishesType import *

class Juicer (KitchenAppliance):
    appliance_type = DishesType.JUICE
    def __init__(self,capacity,power,name,compatibility,dishes):
        self.capacity = capacity
        KitchenAppliance.__init__(self,power,name,compatibility,dishes)

    def __str__(self):
        return "Name : " + str(self.name) + " power: " + str(self.power) + " capacity is " + str(self.capacity) + "  compatibility - " + str(self.compatibility)
