from KitchenAppliance import *
from DishesType import *

class Toaster (KitchenAppliance):
    appliance_type = DishesType.BREAD
    def __init__(self,cookingTime,power,name,compatibility,dishes):
        self.cookingTime = cookingTime
        KitchenAppliance.__init__(self,power,name,compatibility,dishes)

    def __str__(self):
        return "Name : " + str(self.name) + " power: " + str(self.power) + " cookingTime is " + str(self.cookingTime)  + " compatibility - " + str(self.compatibility)
